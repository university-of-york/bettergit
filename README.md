# Setting Yourself Up
Before you start, it's a good idea to set up git to know who you are, as it
stores information about you in commits so people can see who authored any given
change:
```
git config --global user.name 'Joe Bloggs'
git config --global user.email joe.bloggs@example.com
```

# General Help
Git is incredibly well-documented.  A few particularly useful sources of
information are:

* `man git <command>` (for help with a specific command)
* https://git-scm.com/doc (for general information)
* https://github.com/k88hudson/git-flight-rules (how to do common complex things)
* https://chris.beams.io/posts/git-commit/ (for commit message guidance)

# Before You Start
You'll need a system to work on with git installed.  On most Linux distributions
this can be installed from your package manager (the package is almost certianly
named `git`).  On managed PCs it's already installed.  If you're using your own
machine, you can find information at
https://git-scm.com/book/en/v2/Getting-Started-Installing-Git.

If you're a Mac user, you can install git using XCode on the command line:
```
xcode-select --install
```

In the GUI, just click "Install".  If you're a Windows user, you can download
and install git-bash.  On many Linux distributions, `git` is available through
the package manager.

for installation instructions.  This course will use the command-line interface
exclusively; GUIs, IDE integrations and shell extensions are available, but the
terminology used in them sometimes varies.  Often they will ship with a
command-line interface built in.

If you wish to use such an interface, you should be able to locate documentation
for it.  Generally, you can fall back on the command-line client for more
complex operations anyway, should the need arise.

# Initialize a New Repository
In this exercise, you'll set up an empty repository and add files to it.  You
might do this if you have an existing project and wish to start using git as a
version control system.

On managed Windows builds, git-bash defaults to H: as the home directory.

First, open a terminal and create a directory to work in, for example
`my-first-project`:

```
mkdir my-first-project
cd my-first-project
```

You're now in a regular, empty directory.  To turn it into a git repository:
```
git init
```

Git tracks everything inside a hidden folder called `.git`, which you don't need
to (and shouldn't) touch.  You can see this if you type:
```
ls -la
```

You can always check the state of your project directory by issuing the command:
```
git status
```

You can see from this that you're on the default main branch called `master`.

**You now have an empty local git repository**

# Making a Commit
Now that you have a git repository, you can add files and track their histories.

## Creating a File
I'll be using the `atom` text editor to edit files, but you may use whatever you
wish (looking at you, `vim` and `emacs` users).

Start a text editor in the current directory:
```
atom .
```

Create a new file, and add some content.  If you don't have any, here's some for
you to copy:
```
#!/bin/sh
x=1
y=0

while [ $x -lt 100 ]
do
  echo "$x"
  y=$x
  x=$((x + y))
done
```

Make sure you save the file.  You might note that this is very much not a
working Fibonacci generator, but save it as `fib` anyway.  If you want to, you
can mark it as executable using `chmod +x fib`, and run it to check the output
is incorrect.

## Staging Changes
In git, you stage changes to build up what you want to commit.  A commit is a
snapshot of content at one moment in time, with some associated information (who
made the commit, when was it made, what came before it).

Check the status of the working directory:
```
git status
```

You'll note there are no changes staged; to stage a new (or changed) file:
```
git add fib
```

You can see that it's been staged if you use `git status` again.

## Committing
To actually commit your changes, type:
```
git commit
```

This will open your default text editor to add a commit message.  Try to keep
your commit messages succinct, but with enough information to be able to review
the project history.  There are guides on making good commits, for example
https://chris.beams.io/posts/git-commit/.

An example for this commit might be:
```
Add initial fibonacci code

This is the code as it existed at the time, and doesn't function correctly.
```

Note that the subject line is short and written in the *imperative* mood.
There's then a blank line to separate it from the body, which can include more
detail.  If you track issues, you might also make reference to them at the
bottom of the commit message.

Normally, subjects should be less than 50 characters, certainly less than 72,
and should not end with a period.  A blank line should follow before any more
descriptive body.  It's good practice to keep commits straightforward, and
commit often rather than *en masse*, as it shows and comments on the development
process more clearly.

Save and exit the text editor, and git will make a commit.  If you don't enter a
message (or don't save it), git will abort the commit but will leave your staged
changes there.  This is useful if you realise you left something out.

**You now have a local git repository containing your code**

## Viewing History
Git commits chain together to form history, including branching and merging.

To view the history, use:
```
git log
```

The output may be displayed using a pager (e.g. less); to exit this, press 'q'.

By default, it'll give you a moderate amount of information.  You can use many
options to change this - see `man git log` for more information (in general, use
`man git <command>` for help with git).

You can see that you have just one commit.

# Making a Change
The fibonacci program doesn't work in its current form, so let's fix it.  To do
this, replace the body of the loop with:
```
do
  echo "$x"
  s=$((x + y))
  y=$x
  x=$s
done
```

Once you've made the change, you can commit it.  You can either stage the file
manually using `git add fib` like before, or (since you've only modified
existing files), you can use the shorthand:
```
git commit -a
```

This commits **all** changes to tracked files, but doesn't add new ones or
delete removed ones.

Make your commit message.  For example:
```
Fix fibonacci generator

The previous value (y) was being overwritten before the sum was taken.
```

Again, save and exit the text editor.

**You now have a local git repository containing your modified code, and you
could revert the change if need be.**

## Viewing Your Change
If you use `git log` again, you'll see the two commits you made.  If you want to
see just the latest change in more detail, you can use:
```
git show
```

This will show you the changes in patch form.

You can specify a branch name, tag or commit hash to `git show` to view the
changes a commit introduced.  Again, if you want to do these things, check out
the `man` page for that command.

There are also useful formatting options for git show, and many of these are the
same as the command `git diff`, which allows you to compare arbitrary revisions
or files.  I often make use of `--ignore-space-change` and `--word-diff`, but
these are somewhat beyond the scope of this course.

**If you are following this guide as part of the workshop, this is the end of
the first exercise.**

---

# File Management
One of the most common things you'll need to do on any project (besides writing
and modifying code) is to move and delete files.  Git provides wrapper commands
for doing this.

## Moving (Renaming) Files
Instead of using `mv src dest` or `mv src1 src2 dir`, you simply prefix this
with `git`.  This only works for files which are either staged or committed.

Say someone suggests that `fib` is ambiguous - you might want to rename this
script:
```
git mv fib fibonacci
```

If you use `git status`, you'll find that the change has been staged.  You then
commit it in the usual way.

Because of the way git tracks renames (it actually
doesn't internally, it figures it out later if you ask it), it's often best to
commit renames *separately* from any modifications to the renamed files.  If
*other* files are modified (e.g. to reference the renamed file), those can be
included in the same commit without introducing any confusion.

Make this change and commit the new code.

## Deleting Files
Often, you'll refactor code and eventually have a module or script which isn't
required any more.  To keep complexity down, it is best to remove defunct code
(that includes blocks which have been commented out!) - to do this, instead of
typing `rm file`, you again prefix the command with `git`:
```
git rm file
```

Again, this is only for files which were already staged or committed.  Note that
git will also remove the working copy!

### Aside: Deleted Data
One thing to keep in mind is that git remembers *everything* and so any code or
files which are removed can be checked out from the history if needed.  This is
useful if you find there was some useful code in the module you'd removed and
you want to recover it.  However, this also presents some challenges.

For example, if you'd committed a file which contained a password or secret key,
deleting it from the repository would be of no help.  The best thing to do in
such circumstances is to change the password in question, or replace the key.

## Ignoring Files
Often, you'll produce files which you never want to commit to the repository,
such as credentials, intermediate files or built executables.  To avoid being
able to accidentally add them (you can add whole directories at a time!) and
stop `git status` reporting on them, you can create a .gitignore file in your
repository.

Normally a single .gitignore is placed in the root of the project, but you can
override the configuration in individual directories if you need to (this is
unusual and is harder to keep track of).  A .gitignore file contains a series of
filename patterns, such as:

* `*.o` - any `.o` file (e.g. built C objects) in any directory of the project
* `/build/program` - exactly the file `build/program` in the project root
* `config?.txt` - matches `config1.txt`, `config2.txt`, `configX.txt`, *etc.*
* `res[0-9].bin` - matches `res0.bin` to `res9.bin`
* `/photos/*` - matches everything directly in `photos` (but not subdirectories)
* `/photos/**` - matches everything within `photos`

If you need to, you can exclude files from matches, so you could do:
```
*.o
!resources/important_file.o
```
This would enable you to ignore any `.o` files except for that particular one.

You can add comments (lines starting with `#`) and blank lines as you wish.

A `.gitignore` file takes effect any time it's present, but it's normal to
commit it along with the project - just do this as with any other file:
```
git add .gitignore
git commit -m 'Ignore object files'
```

# Reverting a Change
Sometimes you make some changes which introduce bugs, or your client or manager
changes their mind about a decision they made several months ago.  Often, these
have knock-on effects which take a lot of effort, but sometimes you can simply
revert a change (for example, the background colour) or make a good start on a
fix by reverting a change.

For example, let's say that after renaming the script in the example to
`fibonacci`, you find that:

* Three of your clients had scripts which called `fib` directly
* Five more have had trouble spelling the full name
* Another two don't want to type the extra characters

On balance, you decide it's best to upset the one person who things `fib` is
ambiguous than to upset these other ten, so you decide to just revert the
rename.  While you could make this change manually, you can get git to do
exactly the reverse of a commit.

First, you need to find the commit where you made the change.  You could do this
with `git log`, and it's able to perform some fairly powerful queries.  One of
the simplest is to search for a commit by its message, for example:
```
git log --grep Rename
```

Note that grep is case sensitive, but you can use regular expressions, for
example `git log --grep '[Rr]ename'`

Once you've found the commit, copy its commit hash.  If you need to type if for
some reason, you can just type some of the characters (at least 4), as long as
there's no other matching commit (git will tell you if it's ambiguous).  If it
does this, it will list other *objects* which match; ignore anything that isn't
a *commit*.

Revert the rename (you'll need to use the commit hash you found before):
```
git revert <your-commit-hash-here>
```

Git will then present you with a commit prompt, where you can edit the message
if you need to.  You might wish to add some information on why you're doing it,
for example:

```
Revert "Rename fib script to remove ambiguity"

Restore the original name of the fibonacci script, as other people
have it in scripts and can't spell it correctly.

This reverts commit 47785afdb57b9ca0a35d83c0954d6a9d9e6c93df.
```

You can also abort the commit at this stage if you remove all of the non-comment
lines, save and exit.

If you now check your history with `git log`, you'll see both the change and the
revert.  This is useful because it shows that the original was restored and why.
It also means you can always recover code when you've changed it back, in case
assumptions or decisions change.

### Aside: Outstanding Changes Blocking `git revert`
If you have uncommitted changes to files in the repository, git will refuse to
perform certain operations, including `revert`.  This is because it might change
files you're working on, or it might need to have you edit them manually in an
intermediate state.

If you need to perform a revert and have uncommitted changes, you can use the
`git stash` command to stash your changes, make the revert, then *pop* your
changes from the stash:
```
git stash
git revert
git stash pop
```

You can do a lot with stash, but remember it's always local and never part of
the repository history, so this is probably sufficient for now.  Just don't
leave work there for any length of time and you'll be fine.

### Aside: Conflicts During `git revert`
If git can't revert a commit cleanly (because other code changes were made),
it'll tell you, and let you sort out the change yourself.  If you can't work it
out and you want to try a different approach (like making a manual change
instead), you can always abort a revert using `git revert --abort`.

**If you are following this guide as part of the workshop, this is the end of
the second exercise.**

---

# Working With Remotes
At this stage, all of your work is still saved locally (either on your device,
or on your networked home directory, where it's backed up).  If you want to
access it elsewhere, collaborate with others or publish your work, you'll need
to use a git *remote*.

You can create git repositories on many platforms like github, gitlab or
bitbucket.  If you use one of these systems to create a new repository that you
want to push an existing local repository into, you'll need to make sure the
repository is created empty and doesn't automatically generate a license or
readme file.

## Optional: Sign up for a Bitbucket account ##

We recommend you sign up for a Bitbucket account for use in the lab.  This
can be added to the university account later.  You could use a different
provider if you wish, but we offer Bitbucket as a standard git platform for
members of the university.

1. Go to https://bitbucket.org/account/signup/ and enter your university email
address, your name and a password (use a different one to your IT account)

2. Check for the verification email and click 'Verify my email address'

3. If you're prompted to log in, click "Log in with Google" to connect your
Google account; you'll then be prompted for the Bitbucket account credentials
you set before

4. Enter your university username as your bitbucket username - this makes it
easy to identify accounts in future

If you weren't prompted to log in, you can connect your Google account on next
sign-in by following the same steps as in step 3.

If you have the means to use two-factor authentication, you can set this up for
Bitbucket in your account settings.

Once you have a Bitbucket account, fill in the form at
https://forms.gle/ynE672ccSEZP483y5 and we'll add it to the university account
after the lab.  Alternatively, contact itsupport@york.ac.uk later on.

## SSH Keys ##
Without SSH keys, you're constrained to reading public repositories or using
HTTPS with a password (it's the password for bitbucket, not your IT account).
This is generally very inconvenient.

SSH keys can be used to authenticate to remote git servers.  You might already
have one, or you might need to generate a new one.  Once you have a key, you
can add it to your account on the remote git service.

On Bitbucket, you can add an SSH key by clicking your avatar and selecting
*Bitbucket Settings* then *SSH Keys*, and clicking *Add key*.

As well as the notes below, there are instructions on using SSH keys available
at https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key.

Note that you should keep they private key file secure; only the public key
should be added to git remotes.

### Linux ###
To generate a new key, use `ssh-keygen -t rsa -b 2048 -f <filename>`, where
<filename> is the name of the private key, for example `~/.ssh/id_rsa`.

To copy the public key, use `cat ~/.ssh/id_rsa.pub` and copy the contents to the
clipboard.

### Mac ###
To generate a new key, use `ssh add -K <filename>`, where <filename> is the name
of the private key, for example `~/.ssh/id_rsa`.

To copy a public key to the clipboard, use `pbcopy < ~/.ssh/id_rsa.pub`, for
example.

### Windows ###
Launch git-bash, and type ssh-keygen.exe.  It will ask you where you want to
store the key (the defaults should be safe).  Copy the contents of id_rsa.pub.

## Creating the Remote
You should be able to find instructions for the main repository providers, for
example:

* https://help.github.com/en/articles/create-a-repo
* https://docs.gitlab.com/ee/gitlab-basics/create-project.html#blank-projects
* https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html

If you're using another online host, either check their documentation or search
for `my-git-provider how to create a repository` or similar.

Go ahead and create a remote for the lab repository.  Call it "fibonacci", and
store it in your own account.

Once you have created the repository, you should find it lists the remote URL to
use.  Usually you'll want to use SSH, but HTTP(s) can be used too.  With SSH you
set up a key pair so you don't have to type your password.  The git protocol is
not suitable for pushing to remotes as it has no authentication, but can be
useful for cloning public repositories.

For SSH, the URL normally looks something like:
```
git@github.com/my-username/my-project.git
```

## Using Your Remote
When you've found it, you can tell your local repository that you want to use a
remote with it:
```
git remote add origin git@github.com/my-username/my-project.git
```

Note that `origin` is the default name used if you cloned a repository, so it's
conventionally the "main" remote.  You can have multiple remotes for complex
workflows; generally you won't need this day-to-day though.

## Pushing to the Remote
The first time you push a branch (that's a chain of commits, by the way), you
need to tell git where to push it.  For example:
```
git push -u origin master
```

This means, "push my *master* branch to the remote called *origin*, and remember
that I want that to be treated as the *upstream* (`-u`) branch".  Git will, by
default, use the same branch name on the remote (you can override this if you
really need to).

When a remote branch is set as the *upstream* for a local branch, it means that
git associates the two and knows that when you pull changes without telling it
anything else, it should pull those changes into the branch you're on.  This is
the same as if you'd cloned an existing repository.  Each local branch can have
a different upstream branch (normally it has the same name).

**You now have a local and remote git repository containing your code.**

## Pulling Changes
If you edit your code elsewhere (or someone else does), you'll need to merge in
those changes with your local repository.  To do this, you use:
```
git pull
```

If you try this now, you'll find there are no changes - if you want to play with
this, you could make edits via the web interface.

If you *pull* and you have no local changes, your local branch will be updated.
If you've already made and committed changes, the changes on the remote will be
merged onto your local branch automatically (unless there are merge conflicts).

Sometimes you want to avoid such a merge, in which case you can use `git pull
--rebase`.  This performs a rebase operation, which *replays* your changes on
top of the remote changes.  There is some added complexity to this, and you
can't have outstanding uncommitted modifications to do it.

### Aside: Pull Configuration
If you want this to be the default for a branch, repository, or globally, you
can set configuration to do this:
```
git config branch.master.rebase      # Just on master in this repository
git config branch.autosetuprebase    # On any branch I check out from a remote
git config pull.rebase true          # Everywhere in this repository
git config --global pull.rebase true # Everywhere!
```

I'd suggest *not* setting any of the above up unless you absolutely need to.  I
tend to use `git pull -r` explicitly.  If you need to unset a configuration
option, use (for example) `git config --unset branch.master.rebase`.  If it's
a global option you want to reset, also specify `--global`.

# Cloning and Collaborating
Just now, we created a repository and pushed it to a remote.  You will often
want to work with existing code, and possibly collaborate with others.  To do
this, you start with a remote repository and create a local one based on it.

## Cloning a Repository
By cloning a repository, you get its history and contents so you can work on a
local copy.  If you have access, you can push your changes.  If not, you'd often
set up a *fork* of the original in a remote repository of your own and work from
there, or you'd create and submit patch files to the author.

## Forking a Repository
A fork is just a copy of a repository.  Many sites support these internally, and
they maintain a link back to the original.  That makes it easier for others to
find forks if they're public, and for the owner of the fork to submit pull
requests.

When you create a fork, you need to have an account on the platform.  If you
need to make a fork on a different platform, you'll have to do it manually (this
is left as an exercise for the reader) and you'll lose the ability to submit
pull requests or for your fork to be found from the original project.

### Creating a Fork: Bitbucket
Go to the project's page (https://bitbucket.org/university-of-york/bettergit)
and then click the `+` in the sidebar, then select `Fork this repository`.  You
need to specify a name for the forked repository.

### Creating a Fork: Github / Gitlab
Go to the project's page and click the `Fork` button in the top right.  You'll
be prompted for a name for the fork.

## Cloning the Fork
A fork behaves just like any other repository (because it is - only the platform
treats it specially).

To clone the fork, go into your home directory and type (replacing the project):
```
git clone git@bitbucket.org/my-fork-project
```

By default, git will check this out using a directory of the same name, in the
example `my-fork-project`.  If you want to override it, you just pass an extra
argument - `git clone git@bitbucket.org/my-fork-project git-training`.

## Working on the Repository
Just edit, commit and push, as you did before.  If you've made changes that you
want integrating into the original repository, you can submit a pull request.

Pull requests essentially ask the repository owner to pull the changes from your
fork into their codebase.  They might merge it without further changes, though
they might ask you to do particular things, such as:

* Making sure all the tests pass
* Adjusting your code to meet their style guide
* Rebasing your changes against their latest code
* Squashing your commits (this means you turn all your modifications into a
  single commit)

They might also refuse your contributions - remember, it's their code!

For information on creating a pull request, check out the platform documentation
or search for `bitbucket create a pull request` or similar.

**If you are following this guide as part of the workshop, this is the end of
the third exercise.**

---

# Branching, Merging and Tagging
Branches let you break off from the main history of your project in order to
keep development separate.  This can be useful because it gives you the ability
to perform bigger changes without losing track of the original code, and makes
it easier for someone else to review your code as they can easily view the
changes your branch introduces.

Tags are a way of naming particular points in the code history, which can be
useful for software versioning.  They can also be used as temporary names for
commits if you're doing something complicated.

## Creating a Branch
Go back to the fibonacci project you were working on, and create a branch:
```
git checkout -b custom-maximum
```

In practice, branch names should be short and descriptive, but might also refer
to a number in an issue tracker, for example `FDCHEM-63`, for shorter branches.

Make a change on your branch - we'll introduce a new parameter to the script.

Add a line before the while loop `MAX="${1:-100}"` - this means *take the first
parameter, or if it's not set, use the value 100*.  Then, modify the `while`
statement so it reads `while [ $x -lt "$MAX" ]`.

Check the program works, both invoking it as `./fib` and by passing a
parameter such like `./fib 300` and check the output is what you'd expect.  Then
commit your change as normal.

## Merging a Branch
First, check out the branch that you want to merge the change into.  Often, this
is `master`:
```
git checkout master
```

In any repository that has a remote, it's good practice to pull any changes
before you merge - otherwise things can get a bit too exciting.

Now, simply tell git to merge your changes.  Note we've used `--no-ff` to make
it generate a merge commit even though there are no changes on `master`:
```
git merge --no-ff custom-maximum
```

## Creating an Annotated Tag
If we wanted to release this, we might want to provide a version number.  One
way to do this is to use a tag.

Create an annotated tag to identify this as version 0.9.1:
```
git tag -a v0.9.1
```

You'll be prompted for a tag description; enter something like "First stable
release".

You can view the information about a tag using git show:
```
git show v0.9.1
```

Here you can see both the tag information as well as the commit the tag refers
to.

## Deleting Branches
Now that you've merged your branch, you can delete it.  You'll usually want to
wait a while before you do this, in case any fixes are needed (these could be
applied to the branch and then re-merged, which can help to show the fix
process).

To delete the branch:
```
git branch -d custom-maximum
```
